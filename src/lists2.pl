
pairKey(X, (Key, Val)) :- X = Key.
pairVal(X, (Key, Val)) :- X = Val.

isMember(X, [X | Xs]) :- !.
isMember(Y, [X | Xs]) :- isMember(Y, Xs).

appendLists(X, [], Z) :- X = Z.
appendLists([Y | Xs], [Y | Ys], Zs) :- appendLists(Xs, Ys, Zs).


mGetKeyTranslations(X, Transl, Key, []) :- !, X = Transl.
mGetKeyTranslations(X, Transl, Key, [(Key, Val) | T]) :- NewTransl = [Val | Transl],
															!,
															mGetKeyTranslations(X, NewTransl, Key, T).
mGetKeyTranslations(X, Transl, Key, [(OtherK, Val) | T]) :-  !,
															 mGetKeyTranslations(X, Transl, Key, T).

getKeyTranslations(X, Key, []) :- X = [].
getKeyTranslations(X, Key, Dict) :- mGetKeyTranslations(X, [], Key, Dict).



% removeElementFromList implement�cija
mRemoveElementFromList(E, R, NewEs, []) :- R = NewEs.
mRemoveElementFromList(E, R, NewEs, [E | Es]) :- !, mRemoveElementFromList(E, R, NewEs, Es).
mRemoveElementFromList(E, R, NewEs, [C | Es]) :- NewRes = [C | NewEs],
													!,
													mRemoveElementFromList(E, R, NewRes, Es). 


removeElementFromList(E, R, []) :- R = [].
removeElementFromList(E, R, Es) :- !, mRemoveElementFromList(E, R, [], Es).


% sarakstu vien�d�ba ignor�jot duplik�tus
listsEqual([], []) :- !.
listsEqual([X | Xs], [Y | Ys]) :- removeElementFromList(X, YsNoX, [Y | Ys]),
									removeElementFromList(X, XsNoX, Xs),
									removeElementFromList(Y, NewYs, YsNoX),
									removeElementFromList(Y, NewXs, XsNoX),
									!,
									isMember(X, [Y | Ys]),
									isMember(Y, [X | Xs]),
									listsEqual(NewXs, NewYs).



removeDuplicates(X, Res, []) :- X = Res.
removeDuplicates(X, Res, [L | Ls]) :- NextRes = [L | Res],
										removeElementFromList(L, NewLs, Ls),
										!,
										removeDuplicates(X, NextRes, NewLs). 



maa([], B, Res, C) :- !, C = Res.
maa([A | As], B, Res, C) :- getKeyTranslations(NextTrans, A, B),
								appendLists(NewTrans, Res, NextTrans),
								maa(As, B, NewTrans, C).
								

aa([], B, C) :- !, C = [].
aa(A, [], C) :- !, C = [].
aa([A | As], B, C) :- maa([A | As], B, [], CWithDupes),
						!,
						removeDuplicates(C, [], CWithDupes).
						
buildListsFromKeyVals(X, Res, Key, []) :- !, X = Res.
buildListsFromKeyVals(X, Res, Key, [Val | Vals]) :- NewResVal = (Key, Val),
														NewRes = [NewResVal | Res],
														!,
														buildListsFromKeyVals(X, NewRes, Key, Vals).				

rev(X, Res, []) :- !, X = Res.
rev(X, Res, [L | Ls]) :- NewRes = [L | Res],
							rev(X, NewRes, Ls).


cleanComposition(X, Res, []) :- rev(RevRes, [], Res), !, X = RevRes.
cleanComposition(X, Res, [C | Cs]) :- isMember(C, Res), cleanComposition(X, Res, Cs).
cleanComposition(X, Res, [C | Cs]) :- !, cleanComposition(X, [C | Res], Cs).


mkk([], B, Res, C) :- cleanComposition(CleanRes, [], Res),
						!,
						C = CleanRes.
mkk([(Key, Val) | As], B, Res, C) :- getKeyTranslations(X, Val, B),
									 	buildListsFromKeyVals(Pairs, [], Key, X),
									 	appendLists(NewRes, Res, Pairs),
									 	!,
									 	mkk(As, B, NewRes, C). 

kk([], B, C) :- !, C = [].
kk(A, [], C) :- !, C = [].
kk(A, B, C) :- mkk(A, B, [], C).


mrr(X, [], Composition, Result) :- !, X = Result.
mrr(X, Base, [], Result) :- !, X = Result.

mrr(X, Base, Composition, Result) :- kk(Base, Composition, NextComp),
										(listsEqual(Base, NextComp); listsEqual(Composition, NextComp)),
										!,
										X = Result.
																				
mrr(X, Base, Composition, Result) :- !, kk(Base, Composition, NextComp),
										appendLists(NewRes, Result, NextComp),
										mrr(X, Base, NextComp, NewRes).

rr([], X) :- !, X = [].
rr(D, X) :- mrr(PartX, D, D, []),
			!,
			appendLists(FullX, D, PartX),
			cleanComposition(X, [], FullX).

						
				
						
						