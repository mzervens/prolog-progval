%General list processing functions
isMember(X, [X | Xs]) :- !.
isMember(Y, [X | Xs]) :- isMember(Y, Xs).

notMember(X, []) :- !.
notMember(X, [Z | Zs]) :- X \== Z,
							notMember(X, Zs).

lastMember(X, [X | []]) :- !.
lastMember(X, [_ | Ys]) :- lastMember(X, Ys).


secondLastMember(X, [X, _ | []]) :- !.
secondLastMember(X, [_ | Ys]) :- secondLastMember(X, Ys).



% true, ja saraksts X ir apak�kopa sarakstam Y
isSubset([X | []], Ys) :- isMember(X, Ys).
isSubset([X | Xs], Ys) :- isMember(X, Ys),
							isSubset(Xs, Ys).


% true, ja sarakstam X nav kop�ga elementa ar Y
notSubset([X | []], Ys) :- notMember(X, Ys).
notSubset([X | Xs], Ys) :- notMember(X, Ys),
							 notSubset(Xs, Ys).



% len implement�cija
mLen(X, XSum, []) :- X is XSum.
mLen(X, XSum, [Y | Ys]) :- NewSum is XSum + 1,
							mLen(X, NewSum, Ys).  

% saraksta garuma predik�ts
len(X, []) :- X is 0.
len(X, Ys) :- mLen(X, 0, Ys).


% numberOfOccurences implement�cija
mNumberOfOccurences(X, Z, OccSum, []) :- X is OccSum.
mNumberOfOccurences(X, Z, OccSum, [Z | Ys]) :- NewOccSum is OccSum + 1,
												mNumberOfOccurences(X, Z, NewOccSum, Ys).
mNumberOfOccurences(X, Z, OccSum, [_ | Ys]) :- mNumberOfOccurences(X, Z, OccSum, Ys).



% Apr��ina elementa Z skaitu sarakst� Ys
numberOfOccurences(X, Z, []) :- X is 0.
numberOfOccurences(X, Z, Ys) :- mNumberOfOccurences(X, Z, 0, Ys).


% getCommonSubset implement�cija
mGetCommonSubset(X, Common, [], Z) :- !, X = Common.
mGetCommonSubset(X, Common, Y, []) :- !, X = Common.
mGetCommonSubset(X, Common, [C | Ys], [C | Zs]) :-  !, 
													NewComm = [C | Common],
													mGetCommonSubset(X, NewComm, Ys, Zs).

mGetCommonSubset(X, Common, [Y | Ys], [Z | Zs]) :-  isMember(Y, Zs),
													!,
													NewComm = [Y | Common],
													mGetCommonSubset(X, NewComm, Ys, [Z | Zs]).

mGetCommonSubset(X, Common, [Y | Ys], [Z | Zs]) :-  isMember(Z, Ys),
													!,
													NewComm = [Z | Common],
													mGetCommonSubset(X, NewComm, [Y | Ys], Zs).
% fallback
mGetCommonSubset(X, Common, [Y | Ys], [Z | Zs]) :- !, mGetCommonSubset(X, Common, Ys, Zs).


% Ieg�st sarakstu Y un Z kop�jo apak�kopu
getCommonSubset(X, Y, []) :- X = [].
getCommonSubset(X, [], Z) :- X = [].
getCommonSubset(X, Y, Z) :- mGetCommonSubset(X, [], Y, Z).


listSubstract(X, Uniqs, [], Zs) :- X = Uniqs.
listSubstract(X, Uniqs, Ys, []) :- X = Uniqs.
listSubstract(X, Uniqs, [Y | Ys], Zs) :- notMember(Y, Zs),
											NewUniqs = [Y | Uniqs],
											listSubstract(X, NewUniqs, Ys, Zs).
% fallback											
listSubstract(X, Uniqs, [Y | Ys], Zs) :- listSubstract(X, Uniqs, Ys, Zs).



listSubstractNonEmpty(X, Uniqs, [], Zs) :- Uniqs \== [],
											!,
											X = Uniqs.											
listSubstractNonEmpty(X, Uniqs, [], Zs) :- Uniqs == [],
											!,
											X = Zs.
listSubstractNonEmpty(X, Uniqs, Y, Z) :- listSubstract(T, Uniqs, Y, Z),
											listSubstractNonEmpty(X, T, [], Z).



% apvieno divus sarakstus kop�
appendLists(X, [], Z) :- X = Z.
appendLists([Y | Xs], [Y | Ys], Zs) :- appendLists(Xs, Ys, Zs).



% Ieg�st sarakstu Y un Z at��ir�go apak�kopu
getDistinctSubset(X, Y, []) :- X = Y.
getDistinctSubset(X, [], Z) :- X = Z.
getDistinctSubset(X, Y, Z) :- getCommonSubset(Comm, Y, Z),
								listSubstract(UniqYs, [], Y, Comm),
								listSubstract(UniqZs, [], Z, Comm),
								appendLists(X, UniqYs, UniqZs).


% removeElementFromList implement�cija
mRemoveElementFromList(E, R, NewEs, []) :- R = NewEs.
mRemoveElementFromList(E, R, NewEs, [E | Es]) :- mRemoveElementFromList(E, R, NewEs, Es).
mRemoveElementFromList(E, R, NewEs, [C | Es]) :- NewRes = [C | NewEs],
													mRemoveElementFromList(E, R, NewRes, Es). 



removeElementFromList(E, R, []) :- R = [].
removeElementFromList(E, R, Es) :- mRemoveElementFromList(E, R, [], Es).


% sarakstu vien�d�ba ignor�jot duplik�tus
listsEqual([], []) :- !.
listsEqual([X | Xs], [Y | Ys]) :- removeElementFromList(X, YsNoX, [Y | Ys]),
									removeElementFromList(X, XsNoX, Xs),
									removeElementFromList(Y, NewYs, YsNoX),
									removeElementFromList(Y, NewXs, XsNoX),
									!,
									isMember(X, [Y | Ys]),
									isMember(Y, [X | Xs]),
									listsEqual(NewXs, NewYs).


listElementOccurences(Multiplier, [], []) :- !.
listElementOccurences(Multiplier, [X | Xs], [X | Ys]) :- numberOfOccurences(XOccInY, X, [X | Ys]),
															numberOfOccurences(XOccInX, X, [X | Xs]),
															listSubstract(NewXs, [], [X | Xs], [X]),
															listSubstract(NewYs, [], [X | Ys], [X]),
															OccMultiplied is XOccInY * Multiplier,
															!,
															OccMultiplied == XOccInX,
															listElementOccurences(Multiplier, NewXs, NewYs).
													
listElementOccurences(Multiplier, [X | Xs], [Y | Ys]) :- numberOfOccurences(XOccInY, X, [Y | Ys]),
															numberOfOccurences(XOccInX, X, [X | Xs]),
															numberOfOccurences(YOccInX, Y, [X | Xs]),
															numberOfOccurences(YOccInY, Y, [Y | Ys]),
															listSubstract(NewXs, [], [X | Xs], [X, Y]),
															listSubstract(NewYs, [], [Y | Ys], [X, Y]),
															OccMultiplied1 is XOccInY * Multiplier,
															OccMultiplied2 is YOccInY * Multiplier,
															!,
															OccMultiplied1 == XOccInX,
															YOccInX == OccMultiplied2,
															listElementOccurences(Multiplier, NewXs, NewYs).


% visi argumenti ir saraksti
% ! saraksts D satur visus tos un tikai tos saraksta A elementus, kas ir elementi ar� sarakst� B;
% !	saraksts E satur visus tos un tikai tos saraksta A elementus, kas nav elementi ne sarakst� C, ne sarakst� D;
% !	saraksts D satur katru taj� iek�autu A elementu tikpat daudz rei�u, k� �is elements ieiet sarakst� A;
% !	saraksts E satur katru taj� iek�autu A elementu tr�s reizes vair�k nek� �is elements ieiet sarakst� A;
% !	citu elementu sarakstos D un E nav
p(A, B, C, D, E) :- is_list(A), is_list(B), is_list(C), is_list(D), is_list(E),
						getCommonSubset(ABCommon, A, B), 
						
						listSubstract(AsubC, [], A, C),
						listSubstract(AsubCD, [], AsubC, D),

						getCommonSubset(ADComm, A, D),
						getCommonSubset(AEComm, A, E),
						
						listSubstract(AsubD, [], A, D),
						listSubstract(ADCommWithDupes, [], A, AsubD),
						
						listSubstract(AsubE, [], A, E),
						listSubstract(AECommWithDupes, [], A, AsubE),
						!,
						
						listsEqual(ABCommon, D), % #1
						listsEqual(AsubCD, E),   % #2
												
						listsEqual(ADComm, D), % #5
						listsEqual(AEComm, E), % #5
						
						listElementOccurences(1, D, ADCommWithDupes), % #3
						listElementOccurences(3, E, AECommWithDupes). % #4
						


